********************************************************************
       M I N I M U M  C A R D I N A L I T Y  M O D U L E
********************************************************************
Original Author: Gabriel Wong
Current Maintainers: Gabriel Wong
Email: gabrielkpwong@gmail.com

********************************************************************
DESCRIPTION:

   Minimum cardinality module extends the paragraph module giving the option to input a minimum number of paragraphs. If a paragraph does not meet the given minimum, an error will be displayed.

********************************************************************

INSTALLATION:

1. Place the entire minimum_cardinality directory into sites modules directory
  (eg sites/all/modules).

2. Enable this module by navigating to:

     Administration > Modules


3) Please read the step by step instructions as an example to use this
   module below:-

a) Install the module.

b) Go to admin/structure page. Click on manage fields of any content type.

c) Any field using the "Paragraph" Field type will now have the added "Minimum Number of values" option added to it when editing the field.

d) The minimum number of values cannot be greater than the maximum number of values.
